// ==UserScript==
// @name        Emby
// @description Emby
// @version     1.0.2
// @author      hyperion
// @namespace   UserScript
// @icon        https://i.imgur.com/PTMvfrd.png
// @downloadURL https://gitlab.com/elhyperion/userscripts/-/raw/main/emby.user.js
// @updateURL   https://gitlab.com/elhyperion/userscripts/-/raw/main/emby.meta.js
// @require     http://code.jquery.com/jquery-3.4.1.min.js
// @match       https://app.emby.media/*
// @match       http*://*:*096/web/index.html*
// @run-at      document-start
// @grant       GM_addStyle
// ==/UserScript==



// Remove 'Get Premiere' Header button
GM_addStyle('.btnHeaderPremiere { display: none ! important; }');

// Remove 'Discover Premiere' Section from home
GM_addStyle('.section-appinfo.verticalSection-cards.verticalSection { display: none; }');
GM_addStyle('.section-appinfo.verticalSection { display: none; }');

// Remove MediaInfo Section from info dialog
GM_addStyle('.audioVideoMediaInfo.verticalSection { display: none; }');

// Remove Delete button from info dialog
GM_addStyle('.emby-button.detailButton.fab.btnDeleteItem { display: none; }');

// Remove 'Download to...' from context menu
GM_addStyle('button.listItem[data-id="sync"] { display: none; }');

// Remove Delete from context menu
GM_addStyle('button.listItem[data-id="delete"] { display: none; }');

// Remove Convert from context menu
GM_addStyle('button.listItem[data-id="convert"] { display: none; }');

// Remove Share from context menu
GM_addStyle('button.listItem[data-id="share"] { display: none; }');

// Remove brutally Trailers Tab button
GM_addStyle('button.emby-button.emby-tab-button:nth-of-type(4) { display: none; }');

// Remove Cast Header button
GM_addStyle('button.headerCastButton { display: none; }');

