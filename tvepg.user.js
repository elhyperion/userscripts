// ==UserScript==
// @name        tvepg
// @description tvepg
// @version     1.0.9
// @author      hyperion
// @namespace   UserScript
// @icon        https://i.imgur.com/LrCeNt6.png
// @downloadURL https://gitlab.com/elhyperion/userscripts/-/raw/main/tvepg.user.js
// @updateURL   https://gitlab.com/elhyperion/userscripts/-/raw/main/tvepg.meta.js
// @require     http://code.jquery.com/jquery-3.4.1.min.js
// @match       https://tvepg.eu/*
// @match       https://americatvguide.com/*
// @match       https://www.ertflix.gr/epg/channel/*
// @match       http://www.anacon.org/app/chans/*
// @match       https://www.lakatamia.tv/app/chans/*
// @match       https://www.arttv.info/p/*
// @include     https://dlhd.tld/*
// @include     https://freestreams-live.tld/*
// @include     https://cricplay2.tld/*
// @include     https://thetvapp.tld/*
// @include     https://photocalltv.tld/*
// @include     *#tvepgloader
// @include     *#tvepgplayer
// @run-at      document-start
// @grant       GM_addStyle
// ==/UserScript==


// Global variables

var VAR_DEBUG = false;

var VAR_HOST = document.location.hostname;

var VAR_ORIGIN = document.location.origin;

var VAR_HREF = document.location.href;

var VAR_ICON = 'https://i.imgur.com/hrp2CSF.png';

var VAR_LOAD_HASH = '#tvepgloader';

var VAR_PLAY_HASH = '#tvepgplayer';


// Global Hosts


var VAR_TVE_HOST = ['tvepg.eu', 'americatvguide.com'];

var VAR_LAK_HOST = ['www.lakatamia.tv', 'www.anacon.org'];

var VAR_HLS_HOST = 'anym3u8player.com';

var VAR_HLS_PATH = '/tv/p.php?url=';

var VAR_ERT_HOST = 'www.ertflix.gr';

var VAR_DHD_HOST = 'dl+hd.+so';

var VAR_FSL_HOST = 'free+streams-+live.+my';

var VAR_CRF_HOST = 'cric+play2.+xyz';

var VAR_TVA_HOST = 'thetv+app.+to';

var VAR_PHC_HOST = 'photocalltv.org';

var VAR_PHC_ID = 'i3fr';


// Global URLs

var VAR_HLS_URL = function (v) { return urlParser(VAR_HLS_HOST, VAR_HLS_PATH + v) };

var VAR_ERT_URL = function (v) { return urlParser(VAR_ERT_HOST, v) };

var VAR_DHD_URL = function (v) { return urlParser(VAR_DHD_HOST, v) };

var VAR_FSL_URL = function (v) { return urlParser(VAR_FSL_HOST, v) };

var VAR_CRF_URL = function (v) { return urlParser(VAR_CRF_HOST, v) };

var VAR_PHC_URL = function (v) { return urlParser(VAR_PHC_HOST, '/#' + VAR_PHC_ID + v) };

var VAR_TVA_URL = function (v) { return urlParser(VAR_TVA_HOST, v) };


// Run functions

epgLoader(); epgPlayer();


function Categories() {
    return [

        {
            'category': 'ΕΛΛΗΝΙΚΑ',
            'link': 'https://tvepg.eu/el/greece/epg',
        },

        {
            'category': 'ΑΘΛΗΤΙΚΑ',
            'link': 'https://tvepg.eu/el/greece/epg/sports',
        },

        {
            'category': 'ΑΘΛΗΤΙΚΑ (ΗΒ)',
            'link': 'https://tvepg.eu/el/united_kingdom/epg/sports',
        },

        {
            'category': 'ΑΘΛΗΤΙΚΑ (ΗΠΑ)',
            'link': 'https://americatvguide.com/en/us/epg/sports',
        },

        {
            'category': 'ΕΙΔΗΣΕΙΣ',
            'link': 'https://tvepg.eu/el/greece/epg/news',
        },

        {
            'category': 'ΚΥΠΡΟΣ',
            'link': 'https://tvepg.eu/el/cyprus/epg',
        },

        {
            'category': 'ΗΠΑ',
            'link': 'https://americatvguide.com/en/us/epg',
        },

        {
            'category': 'ΑΓΓΛΙΑ',
            'link': 'https://tvepg.eu/el/united_kingdom/epg',
        },

        {
            'category': 'ΔΙΑΦΟΡΑ',
            'link': 'https://' + VAR_DHD_URL() + '/24-7-channels.php' + hash(VAR_DHD_HOST),
        },

    ];
}


function el_links() {
    return [

        {
            'channel': '/el/greece/channel/ert1',
            'link1': VAR_ERT_URL('/epg/channel/ept1-live'),
        },

        {
            'channel': '/el/greece/channel/ert2',
            'link1': VAR_ERT_URL('/epg/channel/ept2-live'),
        },

        {
            'channel': '/el/greece/channel/ert3',
            'link1': VAR_ERT_URL('/epg/channel/ept3-live-2'),
        },

        {
            'mirror': '/el/greece/channel/ert3',
            'mirror1': {
                'title': 'ERT NEWS',
                'name': 'ERT NEWS',
                'logo': 'https://i.imgur.com/BMOPmYg.png',
                'link': VAR_ERT_URL('/epg/channel/ert-news'),
            },
            'mirror2': {
                'title': 'ERT SPORTS',
                'name': 'ERT SPORTS',
                'logo': 'https://i.imgur.com/3CSfT3F.png',
                'link': VAR_ERT_URL('/epg/channel/ert-sports-live-ww'),
            },
            'mirror3': {
                'title': 'ERT MUSIC',
                'name': 'ERT MUSIC',
                'logo': 'https://i.imgur.com/BV0IpAn.png',
                'link': VAR_ERT_URL('/epg/channel/music-tv'),
            },
            'mirror4': {
                'title': 'ERT KIDS',
                'name': 'ERT KIDS',
                'logo': 'https://i.imgur.com/H7OqS2V.png',
                'link': VAR_ERT_URL('/epg/channel/ert-kids-live'),
            },
        },

        {
            'channel': '/el/greece/channel/ant1',
            'link1': 'https://watch.antennaplus.gr/#/live/ant1',
            'link1': VAR_HLS_URL('https://d1nfykbwa3n98t.cloudfront.net/out/v1/6e5667da5a6843899a337dea72adb61b/antenna.m3u8'),
        },

        {
            'channel': '/el/greece/channel/star',
            'link1': 'https://www.star.gr/tv/live-stream',
            'link1': VAR_HLS_URL('https://livestar.siliconweb.com/media/star4/star4.m3u8'),
            'link2': 'https://www.lakatamia.tv/app/chans/gr/stargr.php',
        },

        {
            'channel': '/el/greece/channel/alphatv',
            'link1': 'https://www.alphatv.gr/live/',
            'link1': VAR_HLS_URL('https://alphatvlive.siliconweb.com/1/Y2Rsd1lUcUVoajcv/UVdCN25h/hls/live/playlist.m3u8'),
        },

        {
            'channel': '/el/greece/channel/skai',
            'link1': 'https://www.skaitv.gr/live',
            'link1': VAR_HLS_URL('https://skai-live-back.siliconweb.com/media/cambria4/index_bitrate2000K.m3u8'),
            'link2': 'http://www.anacon.org/app/chans/gr/skaiimage.php',
        },

        {
            'channel': '/el/greece/channel/open_beyond',
            'link1': 'https://www.tvopen.gr/live',
            'link1': VAR_HLS_URL('https://liveopencloud.siliconweb.com/1/ZlRza2R6L2tFRnFJ/eWVLSlQx/hls/live/playlist.m3u8'),
        },

        {
            'channel': '/el/greece/channel/makedonia_tv',
            'link1': 'https://watch.antennaplus.gr/#/live/maktv',
            'link1': VAR_HLS_URL('https://dlm34ll53zqql.cloudfront.net/out/v1/d4177931deff4c7ba994b8126d153d9f/maktv.m3u8'),
        },

        {
            'channel': '/el/greece/channel/mega_tv',
            'link1': 'https://www.megatv.com/live/',
            'link1': 'https://embed.vindral.com/?core.channelId=alteregomedia_megatv1_ci_6cc490c7-e5c6-486b-acf0-9bb9c20fa670',
        },

        {
            'channel': '/el/greece/channel/one_channel',
            'link1': 'https://onechannel.gr/live-tv/',
            'link1': VAR_HLS_URL('https://push-int.siliconweb.com/one/stream/playlist.m3u8'),
        },

        {
            'channel': '/el/greece/channel/action_24',
            'link1': 'https://www.action24.gr/live-stream/',
            'link1': VAR_HLS_URL('https://actionlive.siliconweb.com/actionabr/actiontv/playlist.m3u8'),
        },

        {
            'channel': '/el/greece/channel/kontra',
            'link1': 'https://kontrachannel.gr/live-tv-kontrachannel.html',
            'link1': VAR_HLS_URL('https://kontralive.siliconweb.com/live/kontratv/playlist.m3u8'),
        },

        {
            'channel': '/el/greece/channel/ert_world',
            'link1': VAR_HLS_URL('https://ertflix.s.llnwi.net/ertlive/ertworld/default/index.m3u8'),
            'link1': VAR_ERT_URL('/epg/channel/ept-world-live'),
        },

        {
            'channel': '/el/greece/channel/tv100_thessaloniki',
            'link1': 'https://fm100.gr/live/tv100',
            'link1': VAR_HLS_URL('https://live.fm100.gr/hls/tv100/index.m3u8'),
        },

        {
            'channel': '/el/greece/channel/ena_channel',
            'link1': 'https://enachannel.gr/webtv/',
            'link1': VAR_HLS_URL('https://til.pp.ua:3365/live/enachannellive.m3u8'),
        },

        {
            'channel': '/el/greece/channel/delta_tv',
            'link1': 'http://213.16.128.6:8101/deltatv/live.php',
            'link1': VAR_HLS_URL('https://billing.tfms.xyz/http://81.171.10.42:554/liveD/DStream.sdp/playlist.m3u8'),
        },

        {
            'channel': '/el/greece/channel/thrakinet',
            'link1': 'https://www.thrakinet.tv/thrakinet-live/',
            'link1': VAR_HLS_URL('https://cdn.onestreaming.com/thrakinettv/thrakinettv/playlist.m3u8'),
        },

        {
            'channel': '/el/greece/channel/kphth_tv',
            'link1': 'http://www.cretetv.gr/live-stream/',
            'link1': VAR_HLS_URL('https://cretetvlive.siliconweb.com/cretetv/liveabr/playlist.m3u8'),
        },

        {
            'channel': '/el/greece/channel/kphth_tv_1',
            'link1': 'https://www.kriti1.gr/',
            'link1': VAR_HLS_URL('https://livetv.streams.ovh:8081/kriti/index.m3u8'),
        },

        {
            'channel': '/el/greece/channel/ionian_channel',
            'link1': 'https://ioniantv.gr/live/',
            'link1': VAR_HLS_URL('https://stream.ioniantv.gr/ionian/live_abr/playlist.m3u8'),
        },

        {
            'channel': '/el/greece/channel/samiaki_tv',
            'link1': 'https://samiaki.tv/',
            'link1': VAR_HLS_URL('https://live.cast-control.eu:443/samiaki/samiaki/playlist.m3u8'),
        },

        {
            'channel': '/el/greece/channel/4e',
            'link1': 'http://www.tv4e.gr/livestreaming.php',
            'link1': VAR_HLS_URL('https://billing.tfms.xyz/http://eu2.tv4e.gr:1935/live/myStream.sdp/playlist.m3u8'),
        },

        {
            'channel': '/el/greece/channel/epirus_tv1',
            'link1': 'https://www.epirustv1.eu/',
            'link1': VAR_HLS_URL('https://til.pp.ua:3928/live/epiruslive.m3u8'),
        },

        {
            'channel': '/el/greece/channel/aigaio_tv',
            'link1': 'https://www.aigaiotv.gr/index.php/joomla-pages-2/livestream',
            'link1': VAR_HLS_URL('https://glb.bozztv.com/glb/ssh101/aigaiotv/index.m3u8'),
        },

        {
            'channel': '/el/greece/channel/vergina_tv',
            'link1': 'https://www.verginanews.gr/web-tv-2/',
            'link1': VAR_HLS_URL('https://ssh101.bozztv.com/ssh101/verginatv/playlist.m3u8'),
        },

        {
            'channel': '/el/greece/channel/kriti_nea_tileorasi',
            'link1': 'https://www.neatv.gr/live-broadcast/',
            'link1': VAR_HLS_URL('https://live.neatv.gr:8888/hls/neatv.m3u8'),
        },

        {
            'channel': '/el/greece/channel/star_kentrikis_elladas',
            'link1': 'https://digitalstar.gr/#live',
            'link1': 'https://geo.dailymotion.com/player/xgqxb.html?video=k5Hfx6eeaTjBE5zRXcn',
        },

        {
            'channel': '/el/greece/channel/tv_rodopi',
            'link1': 'https://www.tvrodopi.gr/',
            'link1': 'https://player.twitch.tv/?channel=tvrodopi&parent=www.tvrodopi.gr&autoplay=true',
        },

        {
            'channel': '/el/greece/channel/star_b_ellados',
            'link1': 'https://www.startvfm.gr/live/',
            'link1': 'https://player.twitch.tv/?channel=tvstarfm&parent=www.startvfm.gr&autoplay=true',
        },

        {
            'channel': '/el/greece/channel/tile_epiloges',
            'link1': 'https://epiloges.tv/tileorasi/',
            'link1': VAR_HLS_URL('https://5d00db0e0fcd5.streamlock.net/7170/7170/playlist.m3u8'),
        },

        {
            'channel': '/el/greece/channel/start_tv',
            'link1': 'https://www.startmediacorfu.gr/',
            'link1': VAR_HLS_URL('https://live.cast-control.eu/StartMedia/StartMedia/playlist.m3u8'),
        },

        {
            'channel': '/el/greece/channel/super_b',
            'link1': 'https://pronewstv.gr/',
            'link1': VAR_HLS_URL('https://pro.free.hr:3887/live/pronewslive.m3u8'),
        },

        {
            'channel': '/el/greece/channel/art',
            'link1': 'https://www.arttv.info/p/art.html',
            'link1': 'https://www.arttv.info/p/art.html#iframe',
        },

        {
            'channel': '/el/greece/channel/rik_sat',
            'link1': VAR_HLS_URL('https://l3.cloudskep.com/cybcsat/abr/playlist.m3u8'),
        },

        {
            'timeshift': -1,
            'channel': '/el/greece/channel/antenna_comedy',
            'link1': 'https://watch.antennaplus.gr/#/live/antcomedy',
            'link1': VAR_HLS_URL('https://d2e65zbw2lm5c4.cloudfront.net/comedy_aws/abr/playlist.m3u8'),
        },

        {
            'timeshift': -1,
            'channel': '/el/greece/channel/antenna_drama',
            'link1': 'https://watch.antennaplus.gr/#/live/antdrama',
            'link1': VAR_HLS_URL('https://d2e65zbw2lm5c4.cloudfront.net/drama_aws/abr/playlist.m3u8'),
        },

        {
            'channel': '/el/greece/channel/nova_sports_news',
            'link1': VAR_DHD_URL('/stream/stream-639.php'),
        },

        {
            'channel': '/el/greece/channel/nova_sports_start',
            'link1': VAR_DHD_URL('/stream/stream-637.php'),
            'link2': VAR_FSL_URL('/nova-sports-start/'),
        },

        {
            'channel': '/el/greece/channel/nova_sports_1',
            'link1': VAR_DHD_URL('/stream/stream-631.php'),
            'link2': VAR_FSL_URL('/nova-sports-gr/'),
        },

        {
            'channel': '/el/greece/channel/nova_sports_2',
            'link1': VAR_DHD_URL('/stream/stream-632.php'),
            'link2': VAR_FSL_URL('/nova-sports-2gr/'),
        },

        {
            'channel': '/el/greece/channel/nova_sports_3',
            'link1': VAR_DHD_URL('/stream/stream-633.php'),
            'link2': VAR_FSL_URL('/nova-sports-3/'),
        },

        {
            'channel': '/el/greece/channel/nova_sports_4',
            'link1': VAR_DHD_URL('/stream/stream-634.php'),
            'link2': VAR_FSL_URL('/nova-sports-4/'),
        },

        {
            'channel': '/el/greece/channel/nova_sports_5',
            'link1': VAR_DHD_URL('/stream/stream-635.php'),
            'link2': VAR_FSL_URL('/nova-sports-5/'),
        },

        {
            'channel': '/el/greece/channel/nova_sports_6',
            'link1': VAR_DHD_URL('/stream/stream-636.php'),
            'link2': VAR_FSL_URL('/nova-sports-6/'),
        },

        {
            'channel': '/el/greece/channel/novasports_premier_league',
            'link1': VAR_DHD_URL('/stream/stream-599.php'),
            'link2': VAR_FSL_URL('/nova-sports-pl/'),
        },

        {
            'channel': '/el/greece/channel/novasports_prime',
            'link1': VAR_DHD_URL('/stream/stream-638.php'),
            'link2': VAR_FSL_URL('/novasportsp/'),
        },

        {
            'channel': '/el/greece/channel/cosmote_sport_1',
            'link1': VAR_DHD_URL('/stream/stream-622.php'),
            'link2': VAR_FSL_URL('/cosmotesport1/'),
        },

        {
            'channel': '/el/greece/channel/cosmote_sport_2',
            'link1': VAR_DHD_URL('/stream/stream-623.php'),
            'link2': VAR_FSL_URL('/cosmotesport2/'),
        },

        {
            'channel': '/el/greece/channel/cosmote_sport_3',
            'link1': VAR_DHD_URL('/stream/stream-624.php'),
            'link2': VAR_FSL_URL('/cosmotesport3/'),
        },

        {
            'channel': '/el/greece/channel/cosmote_sport_4',
            'link1': VAR_DHD_URL('/stream/stream-625.php'),
            'link2': VAR_FSL_URL('/cosmote-sport-4/'),
        },

        {
            'channel': '/el/greece/channel/cosmote_sport_5',
            'link1': VAR_DHD_URL('/stream/stream-626.php'),
            'link2': VAR_FSL_URL('/cosmote-sport-5/'),
        },

        {
            'channel': '/el/greece/channel/cosmote_sport_6',
            'link1': VAR_DHD_URL('/stream/stream-627.php'),
            'link2': VAR_FSL_URL('/cosmote-sport-6/'),
        },

        {
            'channel': '/el/greece/channel/cosmote_sport_7',
            'link1': VAR_DHD_URL('/stream/stream-628.php'),
            'link2': VAR_FSL_URL('/cosmote-sport-7/'),
        },

        {
            'channel': '/el/greece/channel/cosmote_sport_8',
            'link1': VAR_DHD_URL('/stream/stream-629.php'),
            'link2': VAR_FSL_URL('/cosmote-sport-8/'),
        },

        {
            'channel': '/el/greece/channel/cosmote_sport_9',
            'link1': VAR_DHD_URL('/stream/stream-630.php'),
            'link2': VAR_FSL_URL('/cosmote-sport-9/'),
        },

        {
            'mirror': '/el/greece/channel/cosmote_sport_9',
            'mirror1': {
                'title': 'ANT1 F1',
                'name': 'ANT1 F1',
                'logo': 'https://i.imgur.com/4AMVBjl.png',
                'link': 'https://watch.antennaplus.gr/#/live/f1',
                'link': VAR_HLS_URL('https://d3r0hoxz2ohm3t.cloudfront.net/out/v1/ff10c34afc0d4a038d0474e51f637649/f1.m3u8'),
            },
        },

        {
            'channel': '/el/greece/channel/vouli',
            'link1': 'https://www.ertflix.gr/epg/channel/boule',
            'link1': VAR_HLS_URL('https://ertflix.s.llnwi.net/ertlive/vouli/default/index.m3u8'),
        },

        {
            'channel': '/el/greece/channel/euronews',
            'link1': 'https://gr.euronews.com/live',
            'link1': 'https://www.ertflix.gr/epg/channel/euronews-gr',
            'link1': 'https://www.youtube.com/embed/uWIhV9gQClg?autoplay=1',
            'link1': VAR_HLS_URL('https://ertflix.s.llnwi.net/ertlive/euronewsgr/default/index.m3u8'),
        },

        {
            'channel': '/el/greece/channel/naftemporiki',
            'link1': 'https://www.naftemporiki.gr/ntv/',
            'link1': VAR_HLS_URL('https://naftemporiki-live.cdn.vustreams.com/live/a4b4a88a-681c-4a2d-8e74-33daa5f2cb61/live.isml/.m3u8'),
        },

        {
            'channel': '/el/greece/channel/cnn',
            'link1': VAR_TVA_URL('/tv/cnn-live-stream/'),
        },

        {
            'channel': '/el/greece/channel/bbc_world_news',
            'link1': VAR_TVA_URL('/tv/bbc-world-news-hd-live-stream/'),
        },

        {
            'mirror': '/el/greece/channel/bbc_world_news',
            'mirror1': {
                'title': 'SKY NEWS',
                'name': 'SKY NEWS',
                'logo': 'https://i.imgur.com/ezYQBzT.png',
                'link': 'https://news.sky.com/watch-live',
                'link': VAR_HLS_URL('https://linear417-gb-hls1-prd-ak.cdn.skycdp.com/100e/Content/HLS_001_1080_30/Live/channel(skynews)/index_1080-30.m3u8'),
            },
        },


        {
            'channel': '/el/greece/channel/dw',
            'link1': 'https://www.ertflix.gr/epg/channel/dw',
            'link1': VAR_HLS_URL('https://ertflix.s.llnwi.net/ertlive/dw/default/index.m3u8'),
        },

        {
            'channel': '/el/greece/channel/france_24_en',
            'link1': 'https://www.france24.com/en/live',
            'link1': 'https://www.youtube.com/embed/Ap-UM1O9RBU?autoplay=1',
        },

        {
            'channel': '/el/greece/channel/france_24_fr',
            'link1': 'https://www.france24.com/fr/direct',
            'link1': 'https://www.youtube.com/embed/l8PMl7tUDIE?autoplay=1',
        },

        {
            'channel': '/el/greece/channel/al_jazeera',
            'link1': 'https://www.aljazeera.com/live',
            'link1': VAR_HLS_URL('https://live-hls-web-aje-fa.getaj.net/AJE/index.m3u8'),
        },

        {
            'channel': '/el/greece/channel/cnbc',
            'link1': VAR_TVA_URL('/tv/cnbc-live-stream/'),
        },

        {
            'channel': '/el/greece/channel/euronews_english',
            'link1': 'https://www.euronews.com/live',
            'link1': 'https://www.ertflix.gr/epg/channel/euronews-english',
            'link1': 'https://www.youtube.com/embed/pykpO5kQJ98?autoplay=1',
            'link1': VAR_HLS_URL('https://ertflix.s.llnwi.net/ertlive/euronewsuk/default/index.m3u8'),
        },

    ];
}


function cy_links() {
    return [

        {
            'channel': '/el/cyprus/channel/rik1',
            'link1': 'https://tv.rik.cy/live-tv/rik-1/',
            'link1': 'http://www.anacon.org/app/chans/cy/rik1cyprus.php',
        },

        {
            'channel': '/el/cyprus/channel/rik2',
            'link1': 'https://tv.rik.cy/live-tv/rik-2/',
            'link1': 'http://www.anacon.org/app/chans/cy/rik2image.php',
        },

        {
            'channel': '/el/cyprus/channel/omega',
            'link1': 'https://www.omegatv.com.cy/live/',
            'link1': 'http://www.anacon.org/app/chans/cy/megacyimage.php',
        },

        {
            'channel': '/el/cyprus/channel/ant1_kuprou',
            'link1': 'https://www.ant1live.com/webtv/live',
            'link1': 'http://www.anacon.org/app/chans/cy/ant1cyprus.php',
            'link2': VAR_HLS_URL('https://l2.cloudskep.com/ant1cm2/abr/playlist.m3u8'),
        },

        {
            'channel': '/el/cyprus/channel/sigma_tv',
            'link1': 'https://www.sigmatv.com/live',
            'link1': 'http://www.anacon.org/app/chans/cy/sigmaimage.php',
            'link2': VAR_HLS_URL('https://l10.cloudskep.com/sigmatv/stv/playlist.m3u8'),
        },

        {
            'channel': '/el/cyprus/channel/alpha_kuprou',
            'link1': 'https://www.alphacyprus.com.cy/live',
            'link1': 'http://www.anacon.org/app/chans/cy/alphacyprus.php',
        },

        {
            'channel': '/el/cyprus/channel/plus_tv',
            'link1': 'http://www.anacon.org/app/chans/cy/pluscyimage.php',
        },

        {
            'channel': '/el/cyprus/channel/capital_tv',
            'link1': 'http://www.anacon.org/app/chans/cy/capitalcyprus.php',
        },

        {
            'channel': '/el/cyprus/channel/extra_tv',
            'link1': 'http://www.anacon.org/app/chans/cy/extracyprus.php',
        },

        {
            'channel': '/el/cyprus/channel/rik_sat',
            'link1': VAR_HLS_URL('https://l3.cloudskep.com/cybcsat/abr/playlist.m3u8'),
        },

        {
            'channel': '/el/cyprus/channel/ert_world',
            'link1': VAR_HLS_URL('https://ertflix.s.llnwi.net/ertlive/ertworld/default/index.m3u8'),
            'link1': VAR_ERT_URL('/epg/channel/ept-world-live'),
        },

        {
            'channel': '/el/cyprus/channel/vouli',
            'link1': 'https://www.ertflix.gr/epg/channel/boule',
            'link1': VAR_HLS_URL('https://ertflix.s.llnwi.net/ertlive/vouli/default/index.m3u8'),
        },

        {
            'channel': '/el/cyprus/channel/4e',
            'link1': 'http://www.tv4e.gr/livestreaming.php',
            'link1': VAR_HLS_URL('https://billing.tfms.xyz/http://eu2.tv4e.gr:1935/live/myStream.sdp/playlist.m3u8'),
        },

        {
            'channel': '/el/cyprus/channel/action_24',
            'link1': 'https://www.action24.gr/live-stream/',
            'link1': VAR_HLS_URL('https://actionlive.siliconweb.com/actionabr/actiontv/playlist.m3u8'),
        },

        {
            'channel': '/el/cyprus/channel/euronews',
            'link1': 'https://gr.euronews.com/live',
            'link1': 'https://www.ertflix.gr/epg/channel/euronews-gr',
            'link1': 'https://www.youtube.com/embed/uWIhV9gQClg?autoplay=1',
            'link1': VAR_HLS_URL('https://ertflix.s.llnwi.net/ertlive/euronewsgr/default/index.m3u8'),
        },

    ];
}


function us_links() {
    return [

        {
            'timeshift': 6,
            'title': 'ABC 7 New York',
            'channel': '/en/us/channel/abc',
            'link1': VAR_DHD_URL('/stream/stream-766.php'),
            'link2': VAR_TVA_URL('/tv/wabc-new-york-abc-east-live-stream/'),
        },

        {
            'timeshift': 6,
            'title': 'CBS 2 New York',
            'channel': '/en/us/channel/cbs',
            'link1': VAR_DHD_URL('/stream/stream-767.php'),
            'link2': VAR_TVA_URL('/tv/wcbs-new-york-cbs-east-live-stream/'),
        },

        {
            'timeshift': 6,
            'title': 'NBC 4 New York',
            'channel': '/en/us/channel/nbc',
            'link1': VAR_DHD_URL('/stream/stream-769.php'),
            'link2': VAR_TVA_URL('/tv/wnbc-new-york-nbc-east-live-stream/'),
        },

        {
            'timeshift': 6,
            'title': 'FOX 5 New York',
            'channel': '/en/us/channel/fox',
            'link1': VAR_DHD_URL('/stream/stream-768.php'),
            'link2': VAR_TVA_URL('/tv/wnyw-new-york-fox-east-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/a_e',
            'link1': VAR_DHD_URL('/stream/stream-302.php'),
            'link2': VAR_TVA_URL('/tv/ae-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/freeform',
            'link1': VAR_DHD_URL('/stream/stream-301.php'),
            'link2': VAR_TVA_URL('/tv/freeform-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/bravo',
            'link1': VAR_DHD_URL('/stream/stream-307.php'),
            'link2': VAR_TVA_URL('/tv/bravo-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/trutv',
            'link1': VAR_DHD_URL('/stream/stream-341.php'),
            'link2': VAR_TVA_URL('/tv/trutv-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/oprah_winfrey_network',
            'link1': VAR_DHD_URL('/stream/stream-331.php'),
            'link2': VAR_TVA_URL('/tv/own-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/fyi',
            'link1': VAR_DHD_URL('/stream/stream-665.php'),
            'link2': VAR_TVA_URL('/tv/fyi-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/pix_11',
            'link1': VAR_DHD_URL('/stream/stream-300.php'),
            'link2': VAR_TVA_URL('/tv/the-cw-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/amc',
            'link1': VAR_DHD_URL('/stream/stream-303.php'),
            'link2': VAR_TVA_URL('/tv/amc-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/bbc_america',
            'link1': VAR_DHD_URL('/stream/stream-305.php'),
            'link2': VAR_TVA_URL('/tv/bbc-america-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/paramount_network',
            'link1': VAR_DHD_URL('/stream/stream-334.php'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/comedy_central',
            'link1': VAR_DHD_URL('/stream/stream-310.php'),
            'link2': VAR_TVA_URL('/tv/comedy-central-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/lifetime',
            'link1': VAR_DHD_URL('/stream/stream-326.php'),
            'link2': VAR_TVA_URL('/tv/lifetime-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/hbo',
            'link1': VAR_DHD_URL('/stream/stream-321.php'),
            'link2': VAR_TVA_URL('/tv/hbo-east-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/hbo_2',
            'link1': VAR_DHD_URL('/stream/stream-689.php'),
            'link2': VAR_TVA_URL('/tv/hbo-2-east-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/hbo_family',
            'link1': VAR_DHD_URL('/stream/stream-691.php'),
            'link2': VAR_TVA_URL('/tv/hbo-family-east-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/hbo_signature',
            'link1': VAR_DHD_URL('/stream/stream-693.php'),
            'link2': VAR_TVA_URL('/tv/hbo-signature-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/hbo_comedy',
            'link1': VAR_DHD_URL('/stream/stream-690.php'),
            'link2': VAR_TVA_URL('/tv/hbo-comedy-hd-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/hbo_zone',
            'link1': VAR_DHD_URL('/stream/stream-694.php'),
            'link2': VAR_TVA_URL('/tv/hbo-zone-hd-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/cinemax',
            'link1': VAR_DHD_URL('/stream/stream-374.php'),
            'link2': VAR_TVA_URL('/tv/cinemax-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/moremax',
            'link1': VAR_TVA_URL('/tv/moremax-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/moviemax',
            'link1': VAR_TVA_URL('/tv/moviemax-live-stream/'),
        },

        {
            'timeshift': 7,
            'channel': '/en/us/channel/showtime',
            'link1': VAR_DHD_URL('/stream/stream-333.php'),
            'link2': VAR_TVA_URL('/tv/showtime-e-live-stream/'),
        },

        {
            'timeshift': 7,
            'channel': '/en/us/channel/showtime_2',
            'link1': VAR_TVA_URL('/tv/showtime-2-live-stream/'),
        },

        {
            'timeshift': 7,
            'channel': '/en/us/channel/starz',
            'link1': VAR_DHD_URL('/stream/stream-335.php'),
            'link2': VAR_TVA_URL('/tv/starz-east-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/fx_movie_channel',
            'link1': VAR_DHD_URL('/stream/stream-381.php'),
            'link2': VAR_TVA_URL('/tv/fx-movie-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/fx',
            'link1': VAR_DHD_URL('/stream/stream-317.php'),
            'link2': VAR_TVA_URL('/tv/fx-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/fxx',
            'link1': VAR_DHD_URL('/stream/stream-298.php'),
            'link2': VAR_TVA_URL('/tv/fxx-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/usa',
            'link1': VAR_DHD_URL('/stream/stream-343.php'),
            'link2': VAR_TVA_URL('/tv/usa-network-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/hallmark_channel',
            'link1': VAR_DHD_URL('/stream/stream-320.php'),
            'link2': VAR_TVA_URL('/tv/hallmark-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/hallmark_movies_mysteries',
            'link1': VAR_DHD_URL('/stream/stream-296.php'),
            'link2': VAR_TVA_URL('/tv/hallmark-movies-mysteries-hd-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/syfy',
            'link1': VAR_DHD_URL('/stream/stream-373.php'),
            'link2': VAR_TVA_URL('/tv/syfy-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/oxygen',
            'link1': VAR_DHD_URL('/stream/stream-332.php'),
            'link2': VAR_TVA_URL('/tv/oxygen-true-crime-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/ion',
            'link1': VAR_DHD_URL('/stream/stream-325.php'),
            'link2': VAR_TVA_URL('/tv/ion-television-east-hd-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/tbs',
            'link1': VAR_DHD_URL('/stream/stream-336.php'),
            'link2': VAR_TVA_URL('/tv/tbs-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/tnt',
            'link1': VAR_DHD_URL('/stream/stream-338.php'),
            'link2': VAR_TVA_URL('/tv/tnt-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/turner_classic_movies',
            'link1': VAR_DHD_URL('/stream/stream-644.php'),
            'link2': VAR_TVA_URL('/tv/tcm-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/sundancetv',
            'link1': VAR_DHD_URL('/stream/stream-658.php'),
            'link2': VAR_TVA_URL('/tv/sundancetv-hd-live-stream/'),
        },

        {
            'timeshift': 7,
            'channel': '/en/us/channel/the_movie_channel',
            'link1': VAR_TVA_URL('/tv/the-movie-channel-east-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/espn',
            'link1': VAR_DHD_URL('/stream/stream-44.php'),
            'link2': VAR_CRF_URL('/espn-usa'),
            'link3': VAR_CRF_URL('/link2/espn-usa'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/espn_2',
            'link1': VAR_DHD_URL('/stream/stream-45.php'),
            'link2': VAR_CRF_URL('/espn2'),
            'link3': VAR_CRF_URL('/link2/espn2'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/espnews',
            'link1': VAR_DHD_URL('/stream/stream-288.php'),
            'link2': VAR_TVA_URL('/tv/espnews-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/espnu',
            'link1': VAR_DHD_URL('/stream/stream-316.php'),
            'link2': VAR_TVA_URL('/tv/espnu-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/golf_channel',
            'link1': VAR_DHD_URL('/stream/stream-318.php'),
            'link2': VAR_TVA_URL('/tv/golf-channel-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/cbs_sports_network',
            'link1': VAR_DHD_URL('/stream/stream-308.php'),
            'link2': VAR_TVA_URL('/tv/cbs-sports-network-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/fs1',
            'link1': VAR_DHD_URL('/stream/stream-39.php'),
            'link2': VAR_TVA_URL('/tv/fox-sports-1-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/fs2',
            'link1': VAR_DHD_URL('/stream/stream-758.php'),
            'link2': VAR_TVA_URL('/tv/fox-sports-2-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/big_ten_network',
            'link1': VAR_DHD_URL('/stream/stream-397.php'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/nfl_network',
            'link1': VAR_DHD_URL('/stream/stream-405.php'),
            'link2': VAR_TVA_URL('/tv/nfl-network-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/nba_tv',
            'link1': VAR_DHD_URL('/stream/stream-404.php'),
            'link2': VAR_TVA_URL('/tv/nba-tv-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/nhl_network',
            'link1': VAR_DHD_URL('/stream/stream-663.php'),
            'link2': VAR_TVA_URL('/tv/nhl-network-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/mlb_network',
            'link1': VAR_DHD_URL('/stream/stream-399.php'),
            'link2': VAR_TVA_URL('/tv/mlb-network-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/tennis_channel',
            'link1': VAR_DHD_URL('/stream/stream-40.php'),
            'link2': VAR_TVA_URL('/tv/tennis-channel-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/fox_soccer_plus',
            'link1': VAR_DHD_URL('/stream/stream-756.php'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/bein_sports_1',
            'link1': VAR_DHD_URL('/stream/stream-425.php'),
        },

        {
            'timeshift': 7,
            'channel': '/en/us/channel/mtv',
            'link1': VAR_DHD_URL('/stream/stream-371.php'),
            'link2': VAR_TVA_URL('/tv/mtv-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/american_heroes_channel',
            'link1': VAR_TVA_URL('/tv/american-heroes-channel-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/science',
            'link1': VAR_DHD_URL('/stream/stream-294.php'),
            'link2': VAR_TVA_URL('/tv/science-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/discovery',
            'link1': VAR_DHD_URL('/stream/stream-313.php'),
            'link2': VAR_TVA_URL('/tv/discovery-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/investigation_discovery',
            'link1': VAR_DHD_URL('/stream/stream-324.php'),
            'link2': VAR_TVA_URL('/tv/investigation-discovery-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/discovery_life',
            'link1': VAR_DHD_URL('/stream/stream-311.php'),
            'link2': VAR_TVA_URL('/tv/discovery-life-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/animal_planet',
            'link1': VAR_DHD_URL('/stream/stream-304.php'),
            'link2': VAR_TVA_URL('/tv/animal-planet-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/national_geographic',
            'link1': VAR_DHD_URL('/stream/stream-328.php'),
            'link2': VAR_TVA_URL('/tv/national-geographic-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/national_geographic_wild',
            'link1': VAR_DHD_URL('/stream/stream-745.php'),
            'link2': VAR_TVA_URL('/tv/nat-geo-wild-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/travel_channel',
            'link1': VAR_DHD_URL('/stream/stream-340.php'),
            'link2': VAR_TVA_URL('/tv/travel-channel-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/history',
            'link1': VAR_DHD_URL('/stream/stream-322.php'),
            'link2': VAR_TVA_URL('/tv/history-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/tlc',
            'link1': VAR_DHD_URL('/stream/stream-337.php'),
            'link2': VAR_TVA_URL('/tv/tlc-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/food_network',
            'link1': VAR_DHD_URL('/stream/stream-384.php'),
            'link2': VAR_TVA_URL('/tv/food-network-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/hgtv',
            'link1': VAR_DHD_URL('/stream/stream-382.php'),
            'link2': VAR_TVA_URL('/tv/hgtv-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/hln',
            'link1': VAR_TVA_URL('/tv/hln-live-stream/'),
        },

        {
            'timeshift': 7,
            'channel': '/en/us/channel/fox_news',
            'link1': VAR_DHD_URL('/stream/stream-347.php'),
            'link2': VAR_TVA_URL('/tv/fox-news-channel-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/cnn',
            'link1': VAR_DHD_URL('/stream/stream-345.php'),
            'link2': VAR_TVA_URL('/tv/cnn-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/cnbc',
            'link1': VAR_DHD_URL('/stream/stream-309.php'),
            'link2': VAR_TVA_URL('/tv/cnbc-live-stream/'),
        },

        {
            'timeshift': 7,
            'channel': '/en/us/channel/bbc_world_news',
            'link1': VAR_TVA_URL('/tv/bbc-world-news-hd-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/fox_business',
            'link1': VAR_DHD_URL('/stream/stream-297.php'),
            'link2': VAR_TVA_URL('/tv/fox-business-network-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/msnbc',
            'link1': VAR_DHD_URL('/stream/stream-327.php'),
            'link2': VAR_TVA_URL('/tv/msnbc-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/the_weather_channel',
            'link1': VAR_DHD_URL('/stream/stream-394.php'),
            'link2': VAR_TVA_URL('/tv/the-weather-channel-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/discovery_family',
            'link1': VAR_DHD_URL('/stream/stream-657.php'),
            'link2': VAR_TVA_URL('/tv/discovery-family-channel-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/boomerang',
            'link1': VAR_DHD_URL('/stream/stream-648.php'),
            'link2': VAR_TVA_URL('/tv/boomerang-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/cartoon_network',
            'link1': VAR_DHD_URL('/stream/stream-339.php'),
            'link2': VAR_TVA_URL('/tv/cartoon-network-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/nick',
            'link1': VAR_DHD_URL('/stream/stream-330.php'),
            'link2': VAR_TVA_URL('/tv/nickelodeon-east-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/nick_jr',
            'link1': VAR_DHD_URL('/stream/stream-329.php'),
            'link2': VAR_TVA_URL('/tv/nick-jr-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/nicktoons',
            'link1': VAR_DHD_URL('/stream/stream-649.php'),
            'link2': VAR_TVA_URL('/tv/nicktoons-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/teennick',
            'link1': VAR_DHD_URL('/stream/stream-650.php'),
            'link2': VAR_TVA_URL('/tv/teennick-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/disney_channel',
            'link1': VAR_DHD_URL('/stream/stream-312.php'),
            'link2': VAR_TVA_URL('/tv/disney-channel-east-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/disney_xd',
            'link1': VAR_DHD_URL('/stream/stream-314.php'),
            'link2': VAR_TVA_URL('/tv/disney-xd-live-stream/'),
        },

        {
            'timeshift': 6,
            'channel': '/en/us/channel/universal_kids',
            'link1': VAR_DHD_URL('/stream/stream-668.php'),
            'link2': VAR_TVA_URL('/tv/universal-kids-live-stream/'),
        },

    ];
}


function uk_links() {
    return [

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/bbc_one',
            'link1': VAR_DHD_URL('/stream/stream-356.php'),
            'link2': VAR_PHC_URL('/bbc?ch=one'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/bbc_two',
            'link1': VAR_DHD_URL('/stream/stream-357.php'),
            'link2': VAR_PHC_URL('/bbc?ch=two'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/bbc_three',
            'link1': VAR_DHD_URL('/stream/stream-358.php'),
            'link2': VAR_PHC_URL('/bbc?ch=three'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/itv1',
            'link1': VAR_DHD_URL('/stream/stream-350.php'),
            'link2': VAR_PHC_URL('/fo?ch=itv1'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/channel_4',
            'link1': VAR_DHD_URL('/stream/stream-354.php'),
            'link2': VAR_PHC_URL('/fo?ch=channel4'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/channel_5',
            'link1': VAR_DHD_URL('/stream/stream-355.php'),
            'link2': VAR_PHC_URL('/fo?ch=channel5'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/itv2',
            'link1': VAR_DHD_URL('/stream/stream-351.php'),
            'link2': VAR_PHC_URL('/fo?ch=itv2'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/bbc_four',
            'link1': VAR_DHD_URL('/stream/stream-359.php'),
            'link2': VAR_PHC_URL('/bbc?ch=four'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/itv3',
            'link1': VAR_DHD_URL('/stream/stream-352.php'),
            'link2': VAR_PHC_URL('/fo?ch=itv3'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_mix',
            'link1': VAR_PHC_URL('/fo?ch=skymix'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/e4',
            'link1': VAR_DHD_URL('/stream/stream-363.php'),
            'link2': VAR_PHC_URL('/fo?ch=e4'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/film4',
            'link1': VAR_DHD_URL('/stream/stream-688.php'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/really',
            'link1': VAR_PHC_URL('/fo?ch=really'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/more4',
            'link1': VAR_PHC_URL('/fo?ch=more4'),
        },

        {
            'timeshift': 1,
            'channel': '/el/united_kingdom/channel/dave_ja_vu',
            'link1': VAR_DHD_URL('/stream/stream-348.php'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/drama',
            'link1': VAR_PHC_URL('/fo?ch=drama'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/5usa',
            'link1': VAR_DHD_URL('/stream/stream-360.php'),
            'link2': VAR_PHC_URL('/fo?ch=5usa'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/itv4',
            'link1': VAR_DHD_URL('/stream/stream-353.php'),
            'link2': VAR_PHC_URL('/fo?ch=itv4'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/itvbe',
            'link1': VAR_PHC_URL('/fo?ch=itvbe'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/5star',
            'link1': VAR_PHC_URL('/fo?ch=5star'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/gold',
            'link1': VAR_DHD_URL('/stream/stream-687.php'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_showcase',
            'link1': VAR_DHD_URL('/stream/stream-682.php'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/legend',
            'link1': VAR_PHC_URL('/fo?ch=legend'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/true_crime_xtra',
            'link1': VAR_PHC_URL('/fo?ch=truecrimextra'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_atlantic',
            'link1': VAR_DHD_URL('/stream/stream-362.php'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_arts',
            'link1': VAR_DHD_URL('/stream/stream-683.php'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_cinema_premiere',
            'link1': VAR_DHD_URL('/stream/stream-671.php'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_cinema_hits',
            'link1': VAR_DHD_URL('/stream/stream-673.php'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_cinema_drama',
            'link1': VAR_DHD_URL('/stream/stream-680.php'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_cinema_family',
            'link1': VAR_DHD_URL('/stream/stream-676.php'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_cinema_action',
            'link1': VAR_DHD_URL('/stream/stream-677.php'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_cinema_comedy',
            'link1': VAR_DHD_URL('/stream/stream-678.php'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_cinema_thriller',
            'link1': VAR_DHD_URL('/stream/stream-679.php'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_cinema_greats',
            'link1': VAR_DHD_URL('/stream/stream-674.php'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_cinema_sci_fi_horror',
            'link1': VAR_DHD_URL('/stream/stream-681.php'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_cinema_animation',
            'link1': VAR_DHD_URL('/stream/stream-675.php'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_cinema_select',
            'link1': VAR_DHD_URL('/stream/stream-672.php'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/viaplay_xtra',
            'link1': VAR_DHD_URL('/stream/stream-597.php'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/viaplay_sports_1',
            'link1': VAR_DHD_URL('/stream/stream-451.php'),
            'link2': VAR_CRF_URL('/viaplay-sports-1'),
            'link3': VAR_CRF_URL('/link2/viaplay-sports-1'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/viaplay_sports_2',
            'link1': VAR_DHD_URL('/stream/stream-550.php'),
            'link2': VAR_CRF_URL('/viaplay-sports-2'),
            'link3': VAR_CRF_URL('/link2/viaplay-sports-2'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/eurosport_1',
            'link1': VAR_DHD_URL('/stream/stream-41.php'),
            'link2': VAR_CRF_URL('/eurosports-1'),
            'link3': VAR_CRF_URL('/link2/eurosports-1'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/eurosport_2',
            'link1': VAR_DHD_URL('/stream/stream-42.php'),
            'link2': VAR_CRF_URL('/eurosports-2'),
            'link3': VAR_CRF_URL('/link2/eurosports-2'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/tnt_sports_1',
            'link1': VAR_DHD_URL('/stream/stream-31.php'),
            'link2': VAR_CRF_URL('/bt-sport-1'),
            'link3': VAR_CRF_URL('/link2/bt-sport-1'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/tnt_sports_2',
            'link1': VAR_DHD_URL('/stream/stream-32.php'),
            'link2': VAR_CRF_URL('/bt-sport-2'),
            'link3': VAR_CRF_URL('/link2/bt-sport-2'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/tnt_sports_3',
            'link1': VAR_DHD_URL('/stream/stream-33.php'),
            'link2': VAR_CRF_URL('/bt-sport-3'),
            'link3': VAR_CRF_URL('/link2/bt-sport-3'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/tnt_sports_4',
            'link1': VAR_DHD_URL('/stream/stream-34.php'),
            'link2': VAR_CRF_URL('/bt-sport-4'),
            'link3': VAR_CRF_URL('/link2/bt-sport-4'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/mutv',
            'link1': VAR_DHD_URL('/stream/stream-377.php'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/lfc_tv',
            'link1': VAR_DHD_URL('/stream/stream-826.php'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_sports_news',
            'link1': VAR_DHD_URL('/stream/stream-366.php'),
            'link2': VAR_CRF_URL('/sky-sports-news'),
            'link3': VAR_CRF_URL('/link2/sky-sports-news'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_sports_main_event',
            'link1': VAR_DHD_URL('/stream/stream-38.php'),
            'link2': VAR_CRF_URL('/sky-sports-main-event'),
            'link3': VAR_CRF_URL('/link2/sky-sports-main-event'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_sports_premier_league',
            'link1': VAR_DHD_URL('/stream/stream-130.php'),
            'link2': VAR_CRF_URL('/sky-sports-premier-league'),
            'link3': VAR_CRF_URL('/link2/sky-sports-premier-league'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_sports_football',
            'link1': VAR_DHD_URL('/stream/stream-35.php'),
            'link2': VAR_CRF_URL('/sky-sports-football'),
            'link3': VAR_CRF_URL('/link2/sky-sports-football'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_sports_cricket',
            'link1': VAR_DHD_URL('/stream/stream-65.php'),
            'link2': VAR_CRF_URL('/sky-sports-cricket'),
            'link3': VAR_CRF_URL('/link2/sky-sports-cricket'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_sports_golf',
            'link1': VAR_DHD_URL('/stream/stream-70.php'),
            'link2': VAR_CRF_URL('/sky-sports-golf'),
            'link3': VAR_CRF_URL('/link2/sky-sports-golf'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_sports_f1',
            'link1': VAR_DHD_URL('/stream/stream-60.php'),
            'link2': VAR_CRF_URL('/sky-sports-f1'),
            'link3': VAR_CRF_URL('/link2/sky-sports-f1'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_sports_action',
            'link1': VAR_DHD_URL('/stream/stream-37.php'),
            'link2': VAR_CRF_URL('/sky-sports-action'),
            'link3': VAR_CRF_URL('/link2/sky-sports-action'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_sports_arena',
            'link1': VAR_DHD_URL('/stream/stream-36.php'),
            'link2': VAR_CRF_URL('/sky-sports-arena'),
            'link3': VAR_CRF_URL('/link2/sky-sports-arena'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_sports_racing',
            'link1': VAR_DHD_URL('/stream/stream-554.php'),
            'link2': VAR_CRF_URL('/sky-sports-racing'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_sports_mix',
            'link1': VAR_DHD_URL('/stream/stream-449.php'),
            'link2': VAR_CRF_URL('/sky-sports-mix'),
            'link3': VAR_CRF_URL('/link2/sky-sports-mix'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/racing_tv',
            'link1': VAR_DHD_URL('/stream/stream-555.php'),
            'link2': VAR_CRF_URL('/racing-uk'),
            'link3': VAR_CRF_URL('/link2/racing-uk'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/mtv',
            'link1': VAR_DHD_URL('/stream/stream-367.php'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/yesterday',
            'link1': VAR_PHC_URL('/fo?ch=yesterday'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_history',
            'link1': VAR_DHD_URL('/stream/stream-686.php'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/true_crime_uk',
            'link1': VAR_PHC_URL('/fo?ch=truecrime'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_crime',
            'link1': VAR_DHD_URL('/stream/stream-685.php'),
        },

        {
            'timeshift': 2,
            'channel': '/el/united_kingdom/channel/sky_witness',
            'link1': VAR_DHD_URL('/stream/stream-361.php'),
        },

    ];
}


function getLinks(){
    let links = [];
    links = links.concat(el_links());
    links = links.concat(cy_links());
    links = links.concat(us_links());
    links = links.concat(uk_links());
    return links
}


function epgLoader(){

    GM_addStyle('.adsbygoogle {display: none !important;}');
    GM_addStyle('.fb-share-button {display: none !important;}');
    GM_addStyle('.twitter-timeline {display: none !important;}');

    if (!hashcatFinder(VAR_HREF, '')) { return };

    iconLoader();

    epgCategories();

    if (VAR_TVE_HOST.includes(VAR_HOST)) {
        GM_addStyle('body {visibility: hidden;}');
        GM_addStyle('.row {left: 0px; margin-top: -25px !important; visibility: visible}');
        GM_addStyle('.menuup {display: none !important;}');
        epgHandler();
    };

    if (hashcatFinder(VAR_HREF, hash(VAR_DHD_HOST))) {
        GM_addStyle('#header {display: none !important;}');
        GM_addStyle('h1 {display: none !important;}');
        hashHandler(hash(VAR_DHD_HOST));
    };

}


function epgPlayer(){

    if (VAR_LAK_HOST.includes(VAR_HOST)) {
        GM_addStyle('.flowplayer {left: 0px; top: 0px; width: 100%; height: 100%; position: fixed !important; visibility: visible}');
        GM_addStyle('.grecaptcha-badge {visibility: hidden}');
        GM_addStyle('center {visibility: hidden}');
        iconLoader();
    };

    if (!hashFinder(VAR_HREF, '')) { return };

    iconLoader();

    if (hashFinder(VAR_HREF, hash(VAR_ERT_HOST))) {
        GM_addStyle('html {width: 100%; height: 100%; position: fixed; visibility: hidden; background-color: black;}');
        GM_addStyle('div[data-name="playerWindowPlace"] {visibility: visible; background-color: black;}');
        GM_addStyle('div[role="dialog"] {visibility: visible}');
        GM_addStyle('#handlePlayer {left: 0px; top: 0px; width: 100%; height: 100%; position: fixed !important; visibility: visible}');
        GM_addStyle('.slick-initialized.slick-slider {display: none !important;}');
    };

    if (hashFinder(VAR_HREF, hash(VAR_DHD_HOST))) {
        GM_addStyle('html {width: 100%; height: 100%; position: fixed; visibility: hidden; background-color: black;}');
        GM_addStyle('iframe[allowfullscreen="true"] {left: 0px; top: 0px; width: 100%; height: 100%; position: fixed !important; visibility: visible}');
    };

    if (hashFinder(VAR_HREF, hash(VAR_CRF_HOST))) {
        GM_addStyle('html {width: 100%; height: 100%; position: fixed; visibility: hidden; background-color: black;}');
        GM_addStyle('iframe {left: 0px; top: 0px; width: 100%; height: 100%; position: fixed !important; visibility: visible}');
    };

    if (hashFinder(VAR_HREF, hash(VAR_FSL_HOST))) {
        GM_addStyle('html {width: 100%; height: 100%; position: fixed; visibility: hidden; background-color: black;}');
        GM_addStyle('iframe[width="100%"] {left: 0px; top: 0px; width: 100%; height: 100%; position: fixed !important; visibility: visible}');
        GM_addStyle('.wpfront-notification-bar-spacer {display: none !important;}');
    };

    if (hashFinder(VAR_HREF, hash(VAR_TVA_HOST))) {
        GM_addStyle('html {width: 100%; height: 100%; position: fixed; visibility: hidden; background-color: black;}');
        GM_addStyle('#my-jwplayer {left: 0px; top: 0px; width: 100%; height: 100%; position: revert; visibility: visible}');
        GM_addStyle('.video-button {left: 0px; top: 0px; width: 100%; height: 100%; position: fixed; visibility: visible}');
    };

    if (hashFinder(VAR_HREF, hash(VAR_PHC_HOST))) {
        let phcorigin = VAR_ORIGIN + '/';

        let phchref = new RegExp(hash(VAR_PHC_HOST) + VAR_PLAY_HASH + '$');
        phchref = VAR_HREF.replace(phchref, '');
        phchref = phchref.replace('#', '');

        document.location.replace(phcorigin);
        document.location.replace(phchref);
    };

    if (hashFinder(VAR_HREF, 'iframe')) {
        GM_addStyle('html {width: 100%; height: 100%; position: fixed; visibility: hidden; background-color: black;}');
        GM_addStyle('iframe {left: 0px; top: 0px; width: 100%; height: 100%; position: fixed !important; visibility: visible}');
    };

}


function hashcatFinder(str, h){

    let hash = h + VAR_LOAD_HASH;

    let match = VAR_HREF.substr(-hash.length,hash.length);

    if (match === hash) { return true };

    return false;
}


function hashFinder(str, h){

    let hash = h + VAR_PLAY_HASH;

    let match = VAR_HREF.substr(-hash.length,hash.length);

    if (match === hash) { return true };

    return false;
}


function hashHandler(h){

    window.onload = function() {

        let hash = h + VAR_PLAY_HASH;

        let anchors = document.getElementsByTagName('a');

        for (let i = 0; i < anchors.length; i++) {

            if (anchors[i].id === VAR_LOAD_HASH) { continue };

            anchors[i].href = anchors[i].href + hash;

        }
    };
}


function hash(h){

    h = h.replace(/[\.\-&\/\\#,+()$~%'":*?<>{}]/g, '');
    h = '#' + h;
    return h
}


function urlParser(h, v){

    let q = hash(h);
    h = h.replace(/[&\/\\#,+()$~%'":*?<>{}]/g, '');
    if (v !== undefined) {
        return 'https://' + h + v + q
    };
    return h
}


function iconLoader(){

    iconHandler();

    $(document).ready( function() {
        iconHandler();
    });
}


function iconHandler(){

    let link = document.createElement('link');
    link.setAttribute('rel', 'shortcut icon');
    link.setAttribute('type', 'image/x-icon');
    link.setAttribute('href', VAR_ICON);

    $('head').append(link);
}


function epgCategories(){

    let div = document.createElement('div');
    div.style.overflow = 'hidden';
    div.style.backgroundColor = '#333';

    Categories().forEach(function(obj) {

        if (obj.category === undefined) {return};

        let a = document.createElement('a');
        a.style.float = 'left';
        a.style.textAlign = 'center';
        a.style.textDecoration = 'none';
        a.style.fontFamily = 'Roboto, Arial, sans-serif';
        a.style.fontSize = '17px';
        a.style.padding = '14px 16px';
        a.style.color = '#f2f2f2';
        a.setAttribute('onmouseover', "this.style.backgroundColor='#04AA6D'");
        a.setAttribute('onmouseout', "this.style.backgroundColor='#333'");
        a.appendChild(document.createTextNode(obj.category));
        a.setAttribute('id', VAR_LOAD_HASH);
        a.href = obj.link + VAR_LOAD_HASH;
        div.appendChild(a);

    });

    document.documentElement.insertBefore(div, document.documentElement.firstChild);
}


function epgHandlerInfo(info){

    if (VAR_DEBUG === false) { return };
    let tab = window.open('');
    tab.document.write(info.join(''));
    tab.document.close();
}


function epgHandler(){

    window.onload = function() {

        var grids = $('li[class="amr-tvgrid-ceil-left"]');

        var rows = $('div[class="mr-tvgrid-row"]');

        if ( (grids.length > 0) && (grids.length !== rows.length) ) {
            return;
        };

        let info = [];

        let links = getLinks();

        let channels = links.map(({channel}) => channel);

        for (let i = 0; i < grids.length; i++) {

            let grid = grids[i]; let row = rows[i];

            // Try to find channel in Program

            let channel = $(grid).find('a').attr('href');

            info.push('<br>'); info.push('-------------------------------');

            if (channel === undefined) {
                info.push('<p style="color:red;font-weight:bold;">No channel found in this grid</p>');
                return;
            };

            // Match found channel in links and remove the Program if no match found

            channel = channel.split(/[?#]/)[0].replace(/\/$/, '');

            if (!channels.includes(channel)) {
                info.push('<p style="color:orange;font-weight:bold;">'+channel+'</p>');
                info.push('<a href="'+VAR_ORIGIN+channel+'" target="_blank">'+VAR_ORIGIN+channel+'</a>');
                $(grid).remove(); $(row).remove(); continue;
            } else {
                info.push('<p style="color:green;font-weight:bold;">'+channel+'</p>');
                info.push('<a href="'+VAR_ORIGIN+channel+'" target="_blank">'+VAR_ORIGIN+channel+'</a>');
            };

            let match = links.find(e => e.channel === channel);

            let mirror = links.find(e => e.mirror === channel);
            if (mirror === undefined) {
                mirror = {};
            };

            if (match === undefined) {
                $(grid).remove(); $(row).remove(); continue;
            };

            // Timeshift Programs (useful for fixing EPGs)

            epgTimer(match, row);
            epgTimer(mirror, row);

            // Create, edit and clone Programs

            info.push('<br>');

            $(grid).find('a').attr('target', '_blank');
            $(row).find('a').attr('target', '_blank');

            let mirrors = [mirror.mirror1, mirror.mirror2, mirror.mirror3, mirror.mirror4, mirror.mirror5];

            mirrors.reverse().forEach(function(m) {

                if (m === undefined) { return };

                info.push('<br><a href="'+m.link+'" target="_blank">'+m.link+'</a>');

                epgCloner(m, m.link + VAR_PLAY_HASH, row, grid);
            });

            let hrefs = [match.link1, match.link2, match.link3];

            hrefs.reverse().forEach(function(href) {

                if (href === undefined) { return };

                info.push('<br><a href="'+href+'" target="_blank">'+href+'</a>');

                epgCloner(match, href + VAR_PLAY_HASH, row, grid);
            });

            $(grid).remove(); $(row).remove();
        };

        epgHandlerInfo(info);
    };

}


function epgCloner(match, href, row, grid){

    let grid2 = $(grid).clone().insertAfter(grid);

    let row2 = $(row).clone().insertAfter(row);

    $(row2).find('a').attr('href', href);

    if ($(row2).find('a').attr('href') === undefined) {
        $(grid2).find('a').attr('href', href);
    };

    if (match.name !== undefined) {

       $(grid2).find('a').attr('title', match.name);
    };

    if (match.logo !== undefined) {

       $(grid2).find('img').attr('src', match.logo);
    };

    if (match.title !== undefined) {

        $(grid2).find('a').attr('href', href);

        $(row2).find('a').each(function() {

            let progTitle = $(this).text();
            progTitle = progTitle.match(/\d+:\d{2}(?:am|pm|)/);
            if (progTitle === null) { return };
            progTitle = progTitle[0] + '  ' + match.title;

            $(this).parent().find('a').attr('title', '');
            $(this).find('span').text(progTitle);
        });
    };

}


function epgTimer(match, row){

    if (match.timeshift === undefined) { return };

    $(row).find('div').attr('class', 'mr-tvgrid-ceil');

    let currentDate = new Date();

    let progs = $(row).find('a');

    progs.each(function(i, obj) {

        try {
            var progStart = $(this).attr('href');
            let dt = progStart.match(/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/);
            progStart = dt[1]+'-'+dt[2]+'-'+dt[3]+' '+dt[4]+':'+dt[5]+':'+dt[6];
            progStart = new Date(progStart);
            progStart.setHours(progStart.getHours() + match.timeshift);
        } catch (error) {
            return;
        }
        try {
            var progEnd = progs.eq(i + 1).attr('href');
            let dt = progEnd.match(/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/);
            progEnd = dt[1]+'-'+dt[2]+'-'+dt[3]+' '+dt[4]+':'+dt[5]+':'+dt[6];
            progEnd = new Date(progEnd);
            progEnd.setHours(progEnd.getHours() + match.timeshift);
        } catch (error) {
            var progEnd = progStart;
        }

        let progTitle = progStart.getHours().toString().padStart(2, '0');
        progTitle += ':';
        progTitle += progStart.getMinutes().toString().padStart(2, '0');
        progTitle += $(this).text().replace(/\d+:\d{2}(?:am|pm|)/, '');

        $(this).parent().find('a').attr('title', '');
        $(this).find('span').text(progTitle);

        let progNow = false;
        if (currentDate > progStart && currentDate < progEnd) {
            progNow = true;
        };
        if (progNow === true) {
            $(this).find('div').attr('class', 'mr-tvgrid-ceil now');
        };
    });

}

