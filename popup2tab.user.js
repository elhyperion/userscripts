// ==UserScript==
// @name         Open pop-up as tab
// @description  Open pop-up as tab
// @version      1.0.1
// @author       Hyperion
// @namespace    UserScript
// @match        *://*/*
// @icon         https://i.imgur.com/z0mFaH6.png
// @downloadURL  https://gitlab.com/elhyperion/userscripts/-/raw/main/popup2tab.user.js
// @updateURL    https://gitlab.com/elhyperion/userscripts/-/raw/main/popup2tab.meta.js
// @run-at       document-start
// @grant        unsafeWindow
// ==/UserScript==


window.addEventListener('click', open, true);
function open(e) {
    const windowOpen = unsafeWindow.open;
    unsafeWindow.open = function(url, target = 'defaultName', windowFeatures) {
        return windowOpen.call(unsafeWindow, url, target, undefined);
    };
}
