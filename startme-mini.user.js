// ==UserScript==
// @name        start.me Mini
// @description start.me Mini
// @version     1.0.2
// @author      hyperion
// @namespace   UserScript
// @icon        https://i.imgur.com/TUfgZ83.png
// @downloadURL https://gitlab.com/elhyperion/userscripts/-/raw/main/startme-mini.user.js
// @updateURL   https://gitlab.com/elhyperion/userscripts/-/raw/main/startme-mini.meta.js
// @match       https://start.me/*
// @run-at      document-start
// @grant       GM_addStyle
// ==/UserScript==


GM_addStyle('.header { visibility: hidden; }');

