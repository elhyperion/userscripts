// ==UserScript==
// @name        start.me
// @description start.me
// @version     1.0.7
// @author      hyperion
// @namespace   UserScript
// @icon        https://i.imgur.com/TUfgZ83.png
// @downloadURL https://gitlab.com/elhyperion/userscripts/-/raw/main/startme.user.js
// @updateURL   https://gitlab.com/elhyperion/userscripts/-/raw/main/startme.meta.js
// @require     http://code.jquery.com/jquery-3.4.1.min.js
// @match       https://start.me/*
// @run-at      document-start
// @grant       GM_addStyle
// ==/UserScript==



// Tweak Large Icon widgets position:
GM_addStyle('.widget-page__container:has(.bookmark-widget__body_size_large) { margin-top: -50px !important; }');
GM_addStyle('.widget-page__container:has(.bookmark-widget__body_size_large) { margin-left: 60px !important; }');
GM_addStyle('.widget-page__container:has(.bookmark-widget__body_size_large) { width: 90% !important; }');

// Do not show widget header controls:
GM_addStyle('.widget-header__controls { display: none !important; }');

// Remove Breadcrumb Label:
GM_addStyle('.breadcrumb__label, .breadcrumb__chevron { display: none !important; }');

// Remove My Pages -> Upgrade for unlimited pages:
GM_addStyle('.pages-sidebar__status { display: none !important; }');

// Remove Share Header button:
GM_addStyle('.header-right__button_type_custom { display: none !important; }');

// Remove Upgrade Header button:
GM_addStyle('.header-right__button_green { display: none !important; }');

// Remove Search Bar in all pages:
GM_addStyle('.widget-page__searchbar { display: none !important; }');

// Remove Google (Custom Search) from Search Bar:
GM_addStyle('.drop-list__item:has([src*="https://f.start.me/start.me"]) { display: none !important; }');

// Remove Pro menus from Widget menu:
GM_addStyle('.actions-list__item_separated:has(.actions-list__premium-mark) { display: none !important; }');
GM_addStyle('.actions-list__item:has(.actions-list__premium-mark) { display: none !important; }');

// Remove Pro menus from Add Widget -> More widgets menu:
GM_addStyle('.subject-overview__item:has(.subject-overview__premium-mark) { display: none !important; }');

// Remove Pro menus from Page Actions menu:
GM_addStyle('.unified-menu-element:has(.premium-mark_clickable) { display: none !important; }');

// Remove Pro menus from Avatar menu -> Settings:
GM_addStyle('.toggler_disabled.toggler_divided.toggler { display: none !important; }');

// Remove Footers:
GM_addStyle('.page-footer { display: none !important; }');
GM_addStyle('.widget-page__ads { display: none !important; }');

