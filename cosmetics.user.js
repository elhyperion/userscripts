// ==UserScript==
// @name        Cosmetics
// @description Cosmetics
// @version     1.0.3
// @author      hyperion
// @namespace   UserScript
// @icon        https://i.imgur.com/ecg8C8w.png
// @downloadURL https://gitlab.com/elhyperion/userscripts/-/raw/main/cosmetics.user.js
// @updateURL   https://gitlab.com/elhyperion/userscripts/-/raw/main/cosmetics.meta.js
// @run-at      document-start
// @grant       GM_addStyle

// @match       *://www.deepl.com/*
// @include     https://dlhd.tld/*
// @match       *://www.footybite.to/*
// @match       *://fullmatchsports.cc/*
// @include     https://www.google.tld/*
// @match       *://www.microsoft365.com/*
// @match       *://www.office.com/*
// @match       *://onedrive.live.com/*
// @match       *://photos.onedrive.com/*
// @match       *://open.spotify.com/*
// @match       *://www.startpage.com/*
// @match       *://app.raindrop.io/*
// @match       *://www.reddit.com/*
// @match       *://trakt.tv/*

// @match       *://www.alphatv.gr/*
// @match       *://watch.antennaplus.gr/*
// @match       *://avclub.gr/*
// @match       *://www.gossip-tv.gr/*
// @match       *://www.makeleio.gr/*
// @match       *://www.sport24.gr/*
// @match       *://www.tvnea.com/*
// @match       *://unboxholics.com/*
// @match       *://www.zappit.gr/*
// ==/UserScript==


var elements = []; var elements2 = [];



elements.push('www.deepl.com##a[href="/pro?cta=header-pro-button"]');
elements.push('www.deepl.com##a[href="/app"]');

elements2.push('dlhd.##div.box:nth-of-type(2)');
elements2.push('dlhd.###sidebar > div.box > center');

elements.push('www.footybite.to##.news-right-sec-div');

elements.push('fullmatchsports.cc##.site-header');
elements.push('fullmatchsports.cc##a[title*="JohnnyBet"]');
elements.push('fullmatchsports.cc##.item.infeed-banner');
elements.push('fullmatchsports.cc###secondary > .widget_custom_html');
elements.push('fullmatchsports.cc##figure[id*="attachment_"]');
elements.push('fullmatchsports.cc##div[class*="post-views"]');
elements.push('fullmatchsports.cc##.entry-content-single.entry-content > p');
elements.push('fullmatchsports.cc##.entry-content-single.entry-content > h2');
elements.push('fullmatchsports.cc##.entry-content-single.entry-content > ul');
elements.push('fullmatchsports.cc##.tab-content > center');
elements.push('fullmatchsports.cc##.btn-container > .kofi-button');
elements.push('fullmatchsports.cc###main > .gmr-box-content');
elements.push('fullmatchsports.cc###footer-container');
elements.push('fullmatchsports.cc##.entry-footer');

elements2.push('www.google.##div[role="contentinfo"]');

elements.push('www.microsoft365.com##button[id="consumer-buy-button"]');
elements.push('www.microsoft365.com##div[id="goPremium_container"]');
elements.push('www.office.com##button[id="consumer-buy-button"]');
elements.push('www.office.com##div[id="goPremium_container"]');

elements.push('onedrive.live.com##div[class*="storageUpsellBanner"]');
elements.push('onedrive.live.com##div[id*="HeaderDiamond"]');
elements.push('photos.onedrive.com##div[class*="purchase-border"]');
elements.push('photos.onedrive.com##a[href*="HeaderDiamond"]');

elements.push('open.spotify.com##button[data-testid="upgrade-button"]');
elements.push('open.spotify.com##a[href="/download"]');

elements.push('www.startpage.com###home-top-banner');
elements.push('www.startpage.com##.below-the-fold');
elements.push('www.startpage.com##.footer-container');
elements.push('www.startpage.com##.css-182a6px');

elements.push('app.raindrop.io##a[class*="upgrade-"]');

elements.push('www.reddit.com##._24UNt1hkbrZxLzs5vkvuDh');

elements.push('trakt.tv##div[class*="grid-item"]:not([data-type])');
elements.push('trakt.tv##[class*="feed-icon"]');
elements.push('trakt.tv##[id="filter-terms-launcher"]');
elements.push('trakt.tv##[id="filter-watchnow-launcher"]');
elements.push('trakt.tv##.btn-list-copy-items');
elements.push('trakt.tv##.btn-list-subscribe');
elements.push('trakt.tv##.btn-list-progress');
elements.push('trakt.tv##.yir-loader');
elements.push('trakt.tv##[href$="/notes"]');
elements.push('trakt.tv##[href$="/widgets"]');
elements.push('trakt.tv##[href$="/vip"]');


elements.push('www.alphatv.gr##.innerBannerContainer > .btnClose');

elements.push('watch.antennaplus.gr##div.toolbar-item > .toggleable-image');
elements.push('watch.antennaplus.gr##.open.modal-container');

elements.push('avclub.gr##.uix_welcomeSection');
elements.push('avclub.gr##center');

elements.push('www.gossip-tv.gr##.wide-banner');

elements.push('www.makeleio.gr##.tn-sidebar-sticky > .widget-area')
elements.push('www.makeleio.gr##.block3-big-thumb-wrap.thumb-wrap');
elements.push('www.makeleio.gr##.clearfix.tn-category-default.tn-block-wrap.block3-wrap');
elements.push('www.makeleio.gr##.text-content-wrapper > p');

elements.push('www.sport24.gr##.adSlot-height--premium.premium_banner.content-thirdParty');
elements.push('www.sport24.gr##.agora_placeholder.creative_placeholder.content-thirdParty');
elements.push('www.sport24.gr##.column--right.column');
elements.push('www.sport24.gr###ros_inline_a');
elements.push('www.sport24.gr###ros_inline_b');
elements.push('www.sport24.gr###ros_article_end');

elements.push('www.tvnea.com###HTML3');
elements.push('www.tvnea.com###HTML4');

elements.push('unboxholics.com##div[id="top-banner"]');

elements.push('www.zappit.gr###inread-video');



styleHandler(elements);

styleHandler2(elements2);


function styleHandler(elements){

    let path = document.location.hostname;

    elements.forEach(function(element) {

        element = element.split('##')

        if (path !== element[0]) { return };

        GM_addStyle(element[1] + ' {display: none !important;}');
    });
}


function styleHandler2(elements){

    let path = document.location.hostname;

    elements.forEach(function(element) {

        element = element.split('##')

        if (!path.startsWith(element[0])) { return };

        GM_addStyle(element[1] + ' {display: none !important;}');
    });
}
